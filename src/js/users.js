import { additionalUsers, randomUserMock } from './mock_for_L3';
import generate from './generate';
import validate from './validator';

function refactorUser(user) {
    return {
        gender: user.gender,
        title: user.name.title,
        full_name: `${user.name.first} ${user.name.last}`,
        city: user.location.city,
        state: user.location.state,
        country: user.location.country,
        postcode: user.location.postcode,
        coordinates: user.location.coordinates,
        timezone: user.location.timezone,
        email: user.email,
        b_day: user.dob.date,
        age: user.dob.age,
        phone: user.phone,
        picture_large: user.picture.large,
        picture_thumbnail: user.picture.thumbnail,
    }
};


function addUserFields(user) {
    let propeties = ['id', 'color', 'course', 'favourite', 'note'];
    propeties.forEach((prop) => {
      if (!(prop in user) || user[prop] === null) {
        user[prop] = generate(prop);
      }
    });
    return user;
};
  
function concatUsers(users, add) {
    const duplicates = users.filter(
      (user) => add.map((addItem) =>
        addItem.postcode + addItem.full_name === user.postcode + user.full_name).filter(Boolean)[0],
    );
    const addWithoutDuplicates = add.filter((addItem) => {
      let isDuplicate = true;
      duplicates.forEach((el) => {
        if (el.full_name === addItem.full_name) {
          isDuplicate = false;
        }
      });
      return isDuplicate;
    });
    return [...users, ...addWithoutDuplicates];
};

function validateUser(user) {
    let isValid = false;
    Object.keys(user).forEach((key) => {
      if(!validate(key, user[key])) {
        isValid = true;
      }
    });
    return isValid;
};
function find(users, param) {
  const properties = ['full_name', 'note', 'age'];
  return users.filter((user) => {
    let isMatch = false;
    properties.forEach((prop) => {
      if (user[prop] === param) {
        isMatch = true;
      }
    });
    return isMatch;
  });
};
function filterUsersByParams(users, filterObj) {
  return users.filter((user) => {
    let isMatch = true;
    Object.keys(filterObj).forEach((prop) => {
      if (user[prop] !== filterObj[prop]) {
        isMatch = false;
      }
    });
    return isMatch;
  });
};
function dynamicSort(property) {
  var sortOrder = 1;
  if(property[0] === "-") {
      sortOrder = -1;
      property = property.substr(1);
  }
  return function (a,b) {
      /* next line works with strings and numbers, 
       * and you may want to customize it to your needs
       */
      if(property == 'b_date') {
        var atime = new Date(a[property]).getTime() / 1000;
        var btime = new Date(b[property]).getTime() / 1000;
        var result = (atime < btime) ? -1 : (atime > btime) ? 1 : 0;
      } else {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
      }
      
      return result * sortOrder;
  }
}

function getPercent(users, params) {
  const filteredUsers = filterUsersByParams(users, params);
  return Math.round((filteredUsers.length / users.length) * 100).toString().concat('%');
};
function initUsers() {
  const refactoredUsers = randomUserMock.map(refactorUser);
  const removedSelfDuplicates = concatUsers(refactoredUsers, refactoredUsers);
  return (concatUsers(removedSelfDuplicates, additionalUsers)).map(addUserFields);
}
  
export default {
    test: () => {
        const users = initUsers();
        console.log(users);
        console.log(find(users, "Foo"));

        console.log(users.filter(validateUser));
        console.log(filterUsersByParams(users, {gender: 'female'}));
        console.log(users.filter(validateUser).sort(dynamicSort('full_name')));
        console.log(getPercent(users, { country: 'Norway', gender: 'female'}));
    }
}