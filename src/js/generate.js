export default function (type) {
    switch(type) {
        case 'id': 
            return Math.random().toString(16).slice(2);
        case 'color':
            return  `#${((1 << 24) * Math.random() | 0).toString(16)}`;
        case 'course':
            const courses = ['Math', 'English', 'PE', 'Music', 'History'];

            return courses[Math.floor(Math.random()*courses.length)];
        case 'favourite':
            return Math.random() < 0.5
        case 'note':
            return "Foo";

    }
}