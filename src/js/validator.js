function validatePhone(phone) {
    if (!(typeof phone == 'string')) {
      return false;
    }
    return phone.match(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im);
}
function validateEmail(email) {
  if (!(typeof email == 'string')) {
    return false;
  }
  return email.toLowerCase().match(
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  );
}
export default function(field, value) {
    if(['full_name', 'gender', 'note', 'state', 'city', 'country'].includes(field)) {
        if (!(typeof value === 'string') && !isUpperCase(value)) {
            return false;
        }
    }
    if (field == 'age') {
        if(typeof value !== 'number') return false;
    }
    
    switch(field) {
      
      case 'phone':
        if(!validatePhone(value)) return false;
      case 'email':
        if(!validateEmail(value)) return false;
    }
    return true;
}